//
// Created by mihai on 13.04.2019.
//

#ifndef MIHAIS_GAME_ASSETSMANAGER_H
#define MIHAIS_GAME_ASSETSMANAGER_H


#include <map>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

class AssetsManager {
    std::map<std::string, std::unique_ptr<sf::Texture>> textures;
    std::map<std::string, std::unique_ptr<sf::SoundBuffer>> sounds;
public:
    static const std::string ASSETS_FOLDER;
    bool loadTexture(std::string name, std::string filePath);
    bool loadSound(std::string name, std::string filePath);
    std::unique_ptr<sf::Texture>& getTexture(std::string name);
    std::unique_ptr<sf::SoundBuffer>& getSound(std::string name);
};


#endif //MIHAIS_GAME_ASSETSMANAGER_H
