#include <SFML/Graphics.hpp>
#define SPEED 0.03f

sf::RectangleShape player(sf::Vector2f(100.0f, 100.0f));

void userInput() {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Up)) {
        player.move(0, -SPEED);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Down)) {
        player.move(0, SPEED);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Left)) {
        player.move(-SPEED, 0);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Right)) {
        player.move(SPEED, 0);
    }
}

int main()
{
    // create the window
    sf::RenderWindow window(sf::VideoMode(800, 600), "Test game");
    player.setFillColor(sf::Color::Red);

    // run the program as long as the window is open
    while (window.isOpen())
    {
        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (window.pollEvent(event))
        {
            switch (event.type) {
                case sf::Event::Closed :
                    window.close();
                    break;
                case sf::Event::KeyPressed :
                    if (event.key.code == sf::Keyboard::Q || event.key.code == sf::Keyboard::Escape) {
                        window.close();
                    }
                    break;
            }
        }

        userInput();

        // clear the window with black color
        window.clear(sf::Color::Black);

        // draw everything here...
        window.draw(player);

        // end the current frame
        window.display();
    }

    return 0;
}
