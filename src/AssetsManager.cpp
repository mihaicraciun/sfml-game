//
// Created by mihai on 13.04.2019.
//

#include "AssetsManager.h"
#include <map>
#include <memory>
#include <iostream>

/** Assets folder location as relative path */
const std::string AssetsManager::ASSETS_FOLDER = "assets";

/** Loads an asset texture into memory */
bool AssetsManager::loadTexture(std::string name, std::string filePath) {
    try {
        if (name.empty() || filePath.empty()) {
            throw std::runtime_error("The name or filepath is null");
        }
        auto texture = std::make_unique<sf::Texture>();
        if (!texture->loadFromFile(filePath)) {
            throw std::runtime_error("The texture could not be loaded");
        }
        textures.insert(std::make_pair(name, std::move(texture)));
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        return false;
    }
    return true;
}

/** Loads an asset sound into memory */
bool AssetsManager::loadSound(std::string name, std::string filePath) {
    try {
        if (name.empty() || filePath.empty()) {
            throw std::runtime_error("The name or filepath is null");
        }
        auto sound = std::make_unique<sf::SoundBuffer>();
        if (!sound->loadFromFile(filePath)) {
            throw std::runtime_error("The sound could not be loaded");
        }
        sounds.insert(std::make_pair(name, std::move(sound)));
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        return false;
    }
    return true;
}

/** Returns an unique pointer reference to a stored sound */
std::unique_ptr<sf::SoundBuffer>& AssetsManager::getSound(std::string name) {
    return sounds[name];
}

/** Returns an unique pointer reference to a stored texture */
std::unique_ptr<sf::Texture>& AssetsManager::getTexture(std::string name) {
    return textures[name];
}