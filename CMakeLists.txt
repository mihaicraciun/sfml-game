cmake_minimum_required(VERSION 3.13)
project(mihais_game)

set(CMAKE_CXX_STANDARD 14)
include_directories(${CMAKE_SOURCE_DIR}/include)

add_executable(mihais_game src/Main.cpp src/AssetsManager.cpp src/AssetsManager.h)
target_link_libraries(mihais_game ${CMAKE_SOURCE_DIR}/lib/libsfml-system.so)
target_link_libraries(mihais_game ${CMAKE_SOURCE_DIR}/lib/libsfml-window.so)
target_link_libraries(mihais_game ${CMAKE_SOURCE_DIR}/lib/libsfml-graphics.so)
target_link_libraries(mihais_game ${CMAKE_SOURCE_DIR}/lib/libsfml-audio.so)